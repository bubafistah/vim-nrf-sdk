#!/bin/bash

# $1 - project root path $2 - NRF SDK root path
make_vimrc(){

	if check_environment; then
		cd "$NRF_PROJECT_DIR"
		echo "Making projects .vimrc"

		# For faster autocomplete
		echo "set complete-=i" > $1/.vimrc

		# Add SDK to project path
		INCLUDABLE_PATHS+=$(find $2/components -maxdepth 3 -type d)
		INCLUDABLE_PATHS+=$(find $2/modules -maxdepth 3 -type d)
		INCLUDABLE_PATHS+=$(find $2/external -maxdepth 3 -type d)
		INCLUDABLE_PATHS+=$(find $2/integration -maxdepth 3 -type d)

		echo "$INCLUDABLE_PATHS" | awk '{print "set path+=" $0;}' >> $1/.vimrc


		# Generate ctags
		echo "Generating sdk ctags"
		cd $2
		ctags -R .
		cd -

		echo "Generate project ctags"
		ctags -R .

		# Append to actual tags
		echo "set tags+=$2/tags" >> $1/.vimrc
		echo "set tags+=./tags" >> $1/.vimrc

		# Preset list of files if multiple matches for tag is found
		echo "nnoremap <C-]> g<C-]>" >> $1/.vimrc
		echo "nnoremap <C-w><C-]> <C-w>g<C-]>" >> $1/.vimrc

		# Add vim internal commands for including folders(for .h files) and .c files
		echo "command NRFINCLUDE !nrf_vim include_in_make %:p" >> $1/.vimrc

		# Vim internal cmd to build project
		echo "command NRFMAKE !nrf_vim make" >> $1/.vimrc

		# Vim internal cmd to build project
		echo "command NRFFLASH !nrf_vim make flash" >> $1/.vimrc

		# Vim internal cmd to build project
		echo "command NRFFLASHFULL !nrf_vim make flash_softdevice flash" >> $1/.vimrc

		# Set tabsizefor consistency
		echo "set tabstop=4" >> $1/.vimrc
		echo "set shiftwidth=4" >> $1/.vimrc
		echo "set expandtab" >> $1/.vimrc
	
	fi
}

pre_make_vimrc(){
	# Only check if project dir was not passed as argument
	if [ "$check_project_dir" -eq "1" ]; then
		# Check if we are in active project dir
		if [ "$(realpath ".")" != "$(realpath $NRF_PROJECT_DIR)" ]; then
			confirm_action "Use $(realpath ".") as project root dir"	
			if [ $? -eq 0 ]; then
				echo "OK"
				NRF_PROJECT_DIR="$(realpath ".")"
			else
				echo "Using $NRF_PROJECT_DIR as project root dir"	
			fi
		fi
	fi
	
	print_config	
	#confirm_action "Local project config"	
}

init_environment(){

	# Check for ctags
	check_installed ctags

	# For ability to include local vim files
	print_if_dont_exist ~/.vimrc "set exrc" >> ~/.vimrc

}

# $1 - what to confirm
confirm_action(){
	echo "__________________________________"
	read -p "$1?[y/N]" -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		return 0
	else
		return 1
	fi
}

# $1 - path/file to include
include(){
	echo "Trying to include $1"

	filename=$(basename $1)
	dirname=$(dirname $1)
	#echo "$filename"
	extension="${filename##*.}"
	#echo "$extension"
	#echo "$dirname"
	
	if [ $extension = 'c' ]; then
		# Add as src file
		#echo "WOW its a C file"
		modified_path="$(filepath_enchant $1)"
		#echo "$modified_path"	
		confirm_action "Add $modified_path to Makefile"
		if [ $? -eq 0 ]; then
			new_file_contents="$(add_source $NRF_PROJECT_MAKE_FILE $modified_path)"
			echo "$new_file_contents" > $NRF_PROJECT_MAKE_FILE
			echo "DONE"
		else
			echo "ABORTED"
		fi
	fi

	if [ $extension = 'h' ]; then

		# Add as inc folder
		#echo "WOW its a H file"
		modified_path="$(filepath_enchant $dirname)"
		#addable="$(print_if_dont_exist $NRF_PROJECT_MAKE_FILE $modified_path)"
		#echo "$addable"
		confirm_action "Add $modified_path to Makefile"
		if [ $? -eq 0 ]; then
			new_file_contents="$(add_include $NRF_PROJECT_MAKE_FILE $modified_path)"
			echo "$new_file_contents" > $NRF_PROJECT_MAKE_FILE
			echo "DONE"
		else
			echo "ABORTED"
		fi
		
	fi


}

# Make glorious path enchantment
filepath_enchant(){
	# Replace SDK folder with make file inner var (it should be defined there)
	enchantment="$(echo "$1" | sed -e 's@'$NRF_PROJECT_DIR'@\$(PROJ_DIR)@g')"
	enchantment="$(echo "$enchantment" | sed -e 's@'$NRF_SDK_DIR'@\$(SDK_ROOT)@g')"
	echo "$enchantment"
	
}

# Add include to Makefile INC_FOLDERS var
# $1 - Makefile path $2 - Dir to add
add_include(){
	 sed "/INC_FOLDERS/a\  ${2} "'\\' $1
}

# Add source to Makefile SRC_FILES var
# $1 - Makefile path $2 - Dir to add
add_source(){
	sed "/SRC_FILES/a\  ${2} "'\\' $1
}

# Searches SDK and projoect folder for files to include
# $1 - file to search .h
suggest_include_h(){
	find -name $1
}

# Opens another terminal and calls make in it
make_project(){
	make_file_dir="$(dirname $NRF_PROJECT_MAKE_FILE)"
	#$NRF_MAKE_TERMINAL -e "cd $make_file_dir;make" # To open in seperate terminal
	echo "Making project"
	echo "$make_file_dir"
	cd $make_file_dir;make "$@" # Call actual make with passed arguments
}

# If project dir is valid call this to interactively select target Makefile
suggest_makefile(){
	readarray suggestions < <(find $NRF_PROJECT_DIR -name Makefile)
	echo "Possible Makefiles:"
	for i in ${!suggestions[@]}; do
		echo "$i. - ${suggestions[$i]}"
	done

	echo -n "Choose target Makefile:"
	read choise
	if [ -f ${suggestions[$choise]} ]; then # Dont work TODO
		echo "Using : ${suggestions[$choise]}"
		NRF_PROJECT_MAKE_FILE=${suggestions[$choise]}
		save_config
		return 0
	else
		echo "Not a valid file"
		exit 1
	fi
}

# $1 - file path $2 - addable
print_if_dont_exist(){
	
	#awk -F @ -v f=0 " \$0 ~ $2 {f=1}END{ print (f==0) ? \"$2\" : \"\" }" $1
	
	awk -v f=0 "/$2/{f=1}END{ print (f==0) ? \"$2\" : \"\" }" $1
}

# $1 -package to check
check_installed(){
	
	if dpkg -l | grep -q $1; then
		echo "$1 found"
	else
		echo "You should install $1"
		exit 1
	fi
}

check_environment(){
	if [ -d "$NRF_SDK_DIR" ]; then
		echo "SDK dir found"
		if [ -d "$NRF_PROJECT_DIR" ]; then # Ignore make for now
			# Totally valid data input
			echo "Project dir found"
			
			if [ -f "$NRF_PROJECT_MAKE_FILE" ]; then # Ignore make for now
				echo "Makefile found"
			else
				echo "No Makefile"
				suggest_makefile
				return $?
			fi
			return 0
		else
			echo "No project directory"
			return 1
		fi
	else
		echo "No SDK path"
		return 1
	fi

}

# Creates persistant storage of env variables
save_config(){
	echo "export NRF_PROJECT_DIR="$NRF_PROJECT_DIR"" > ~/.nrf_vim
	echo "export NRF_SDK_DIR="$NRF_SDK_DIR"" >> ~/.nrf_vim
	echo "export NRF_PROJECT_MAKE_FILE="$NRF_PROJECT_MAKE_FILE"" >> ~/.nrf_vim
	echo "export NRF_MAKE_TERMINAL="$NRF_MAKE_TERMINAL"" >> ~/.nrf_vim
}

# Load persistant var storage
load_config(){
	if [ -e ~/.nrf_vim ]; then
		source ~/.nrf_vim
	else
		touch ~/.nrf_vim
	fi

}

print_config(){
	echo "Dumping current config:"
	echo "NRF_PROJECT_DIR="$NRF_PROJECT_DIR""
	echo "NRF_SDK_DIR="$NRF_SDK_DIR""
	echo "NRF_PROJECT_MAKE_FILE="$NRF_PROJECT_MAKE_FILE""
	echo "NRF_MAKE_TERMINAL="$NRF_MAKE_TERMINAL""

}

init_command(){
	if check_environment;  then
		init_environment
		save_config
	fi
}

#add_include
# Write to actual Makefile
#add_include > $MAKE
#add_include $MAKE_FILE $APPENDABLE
#print_if_dont_exist $MAKE_FILE

NRF_PROJECT_MAKE_FILE=''
NRF_SDK_DIR=''
NRF_PROJECT_DIR=''

check_project_dir=1
check_sdk_dir=1

# Should be modifieable
NRF_MAKE_TERMINAL='i3-sensible-terminal'
save_config=1

load_config

#target_command=$1
while getopts "ip:s:m:h" opt; do
	case "$opt" in
		p)
			check_project_dir=0
			NRF_PROJECT_DIR="$(realpath "$OPTARG")"
			# Reset makefile
			NRF_PROJECT_MAKE_FILE=''
		;;
		s)  
			NRF_SDK_DIR="$(realpath "$OPTARG")"
		;;
		m)
			NRF_PROJECT_MAKE_FILE=$OPTARG
		;;
		h)
			help=" $(basename "$0")  [-p -s -m -h] [COMMAND]
				COMMANDS:
					init - init nrf dev environment with target directories, if run without args - uses default from ~./.$(basename "$0")
				PARAMS:
					-p nRF project dir root 
					-s nRF target SDK root dir 
					-m Target make file location
					-h Print this
			"
			echo "$help"
			exit 0
		;;
		*)
			echo "emm"
		;;
	esac
done
shift $((OPTIND -1))

# Save if something changed
save_config

target_command=$1

# Get target command
case "$target_command" in
	init)
		echo "Initing stuff"
		init_command "$NRF_PROJECT_DIR"
	;;
	gen_vimrc)
		echo "Generating project local .vimrc"
		$2
		pre_make_vimrc
		make_vimrc $NRF_PROJECT_DIR $NRF_SDK_DIR
	;;
	self_install)
		cp ./nrf_vim /usr/bin
	;;
	include_in_make)
		include $2	
	;;
	suggest)
		suggest_makefile
	;;
	print_config)
		print_config
	;;
	make)
		make_project "${@:2}"
	;;
	*)
		echo "No action specified"
		echo "$help"

	;;
esac
