Paths should be defines without trailing backslash
```
# Install
./nrf_vim self_install
nrf_vim -s ~/nRF_SDK_DIR_xx.x init

# Use
cd my_project_root
nrf_vim gen_vimrc

# Open vim from project_root
vim
```

